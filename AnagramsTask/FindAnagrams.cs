using System.Collections.Generic;
using System.Linq;

namespace AnagramsTask
{
    public class FindAnagrams
    {
        private IList<string> _input;
        private Dictionary<string, IList<string>> _dictionaryAnagrams;
        
        public FindAnagrams(IList<string> words)
        {
            _input = words;
            _dictionaryAnagrams = new Dictionary<string, IList<string>>();
        }

        public IList<IList<string>> GetGroupedAnagrams()
        {
            foreach (var word in _input)
            {
                var containKey = _dictionaryAnagrams.Keys.FirstOrDefault(x => _isAnagrams(word, x));
                if (string.IsNullOrEmpty(containKey))
                {
                    _dictionaryAnagrams.Add(word, new List<string>{word});
                }
                else
                {
                    _dictionaryAnagrams[containKey].Add(word);
                }
            }

            return _dictionaryAnagrams.Select(x => x.Value).ToList();
        }

        private bool _isAnagrams(string word1, string word2)
        {
            if (word1.Length != word2.Length)
            {
                return false;
            }

            var aFrequency = _getFrequency(word1);
            var bFrequency = _getFrequency(word2);

            foreach (var key in aFrequency.Keys)
            {
                if (!bFrequency.ContainsKey(key)) return false;
                if (aFrequency[key] != bFrequency[key]) return false;
            }

            return true;
        }

        private Dictionary<char, int> _getFrequency(string word)
        {
            var dict = new Dictionary<char, int>();

            foreach (var @char in word)
            {
                if (!dict.ContainsKey(@char))
                {
                    dict.Add(@char, 0);
                }

                dict[@char]++;
            }

            return dict;
        }
    }
}