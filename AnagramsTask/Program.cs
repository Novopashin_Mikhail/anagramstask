﻿using System;

namespace AnagramsTask
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = new[] {"tor", "rot", "rto", "otr", "of", "fo", "foo", "gavr", "avgr"};
            var findAnagrams = new FindAnagrams(input);
            var output = findAnagrams.GetGroupedAnagrams();
            foreach (var item in output)
            {
                Console.Write("[");
                foreach (var innerItem in item)
                {
                    Console.Write($"{innerItem},");
                }
                Console.Write("],");
            }
        }
    }
}